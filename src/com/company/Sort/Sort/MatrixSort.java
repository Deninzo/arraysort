package com.company.Sort.Sort;

import java.util.ArrayList;

public class MatrixSort extends MainSort {
    ArrayList<ArrayList<Integer>> matrix;
    int len = 0;
    public MatrixSort(ArrayList<ArrayList<Integer>> matrix){
        this.matrix = matrix;
        int size = matrix.size() * matrix.get(0).size();
        len = matrix.get(0).size();
        int[] mas = new int[size];
        int count = 0;
        for(int i = 0; i < matrix.size(); i++){
            ArrayList<Integer> prom = matrix.get(i);
            for(int j = 0 ; j<len; j++){
                mas[count] = prom.get(j);
                count++;
            }
        }
        Node first = new Node(mas[0]);
        for (int i = 1; i < mas.length; i++) {
            first.add(new Node(mas[i]));
        }
        res = new ArrayList<>();
        first.get(res);
    }

    public void doMatrixSortYbiv(){
        print();
    }
    public void doMatrixSortVozr(){
        for (int i = res.size() - 1; i >= 0; i--) {

            System.out.print(res.get(i)+" ");
            if(i%len==0){
                System.out.println();
            }
        }
        System.out.println();
    }

    public void deleteMax(){
        int max = 0;
        for (int i = 0; i < matrix.size(); i++) {
            ArrayList<Integer> prom = matrix.get(i);
            for (int j = 0; j < prom.size(); j++) {
                if(prom.get(j)>max){
                    max = prom.get(j);
                }
            }
        }
        for (int i = 0; i < matrix.size(); i++) {
            ArrayList<Integer> del = matrix.get(i);
            for (int j = 0; j < del.size(); j++) {
                if(del.get(j)==max){
                    del.remove(j);
                }
            }
        }
        print();

    }

    public void print(){
        for (int i = 0; i < matrix.size() ; i++) {
            ArrayList<Integer> list = matrix.get(i);
            for (int j = 0; j < list.size(); j++) {
                System.out.print(list.get(j)+" ");
            }
            System.out.println();
        }
    }

}
