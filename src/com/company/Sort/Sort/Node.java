package com.company.Sort.Sort;

import java.util.List;

public class Node {
    private boolean flag = true;
    private int value;
    private int count = 1;
    private Node parent;
    private Node left;
    private Node right;

    public Node(int value) {
        this.value = value;
    }

    public void add(Node add) {
        if (this.value > add.value && flag) {
            if (this.right != null) {
                right.add(add);
            } else {
                add.setParent(this);
                right = add;
            }
        } else if (this.value < add.value && flag) {
            if (this.left != null) {
                left.add(add);
            } else {
                add.setParent(this);
                left = add;
            }
        } else {
            count++;
        }
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void get(List<Integer> list) {
        if (this.getLeft() != null) {
            this.getLeft().get(list);
        }
        for (int i = 0; i < this.getCount(); i++) {
            list.add(this.getValue());
        }
        if (this.getRight() != null) {
            this.getRight().get(list);
        }
    }

    public int getCount() {
        return count;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}
