package com.company.Sort;

import com.company.Sort.FindNumber.Find;
import com.company.Sort.Sort.MainSort;
import com.company.Sort.Sort.MatrixSort;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
//        int[] mas = new int[in.nextInt()];
//        for (int i = 0; i < mas.length; i++) {
//            mas[i] = in.nextInt();
//        }
//        MainSort sort = new MainSort(mas);
//        System.out.println("Сортировка массива по возрастанию");
//        sort.doSortPoVozr();
//        System.out.println("\nСортировка массива по убыванию");
//        sort.doSortPoYbiv();
//        System.out.println("\nНахождение числа, в котором цифры идут строго по возрастанию");
//        Find findd = new Find();
//        System.out.println(findd.findNumber(mas));
//        System.out.println("Вывод месяца по его номеру");
//        findd.writeMonth(14);
//        System.out.println("Сортировка матрицы по убыванию");
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        ArrayList<Integer> mas1 = new ArrayList<>();
        ArrayList<Integer> mas2 = new ArrayList<>();
        ArrayList<Integer> mas3 = new ArrayList<>();
        mas1.add(1);
        mas1.add(4);
        mas1.add(43);
        mas1.add(63);
        mas2.add(53);
        mas2.add(34);
        mas2.add(63);
        mas2.add(63);
        mas3.add(63);
        mas3.add(61);
        mas3.add(61);
        mas3.add(60);
        list.add(mas1);
        list.add(mas2);
        list.add(mas3);
        MatrixSort mtr = new MatrixSort(list);
//        mtr.doMatrixSortYbiv();
//        System.out.println();
//        mtr.doMatrixSortVozr();
        mtr.deleteMax();
    }
}
